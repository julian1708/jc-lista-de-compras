package com.example.buysjcwilfer.modelos;

import java.io.Serializable;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

public class Product extends RealmObject implements Serializable {

    @PrimaryKey
    private String id;
    private String product;
    private String description;
    private float quantity;
    private int price;

    public Product() {
    }

    public Product(String id, String product, String description, float quantity, int price) {
        this.id = id;
        this.product = product;
        this.description = description;
        this.quantity = quantity;
        this.price = price;
    }

    public String getId() { return id; }

    public void setId(String id) { this.id = id; }

    public String getProduct() { return product; }

    public void setProduct(String product) { this.product = product; }

    public String getDescription() { return description; }

    public void setDescription(String description) { this.description = description; }

    public float getQuantity() { return quantity; }

    public void setQuantity(float quantity) { this.quantity = quantity; }

    public int getPrice() { return price; }

    public void setPrice(int price) { this.price = price;}
}

