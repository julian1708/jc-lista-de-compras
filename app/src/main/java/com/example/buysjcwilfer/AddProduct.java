package com.example.buysjcwilfer;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.buysjcwilfer.modelos.Product;

import java.util.UUID;

import io.realm.Realm;

public class AddProduct extends AppCompatActivity {

    EditText etProduct;
    EditText etDescription;
    EditText etQuantity;
    EditText etPrice;

    Button btnSave;
    Button btnCancel;

    Realm realm;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_product2);
        loadViews();
        realm = Realm.getDefaultInstance();
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getSupportActionBar() != null){
            getSupportActionBar().setTitle(R.string.addProduct);
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                }
            });
        }
    }

    private void loadViews() {
        etProduct = findViewById(R.id.et_product);
        etDescription = findViewById(R.id.et_description);
        etQuantity = findViewById(R.id.et_quantity);
        etPrice = findViewById(R.id.et_price);

        btnSave = findViewById(R.id.btn_save);
        btnCancel = findViewById(R.id.btn_cancel);

        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                save(view);
            }
        });
    }

    public void save(View view){
        String productName = etProduct.getText().toString().trim();
        String description = etDescription.getText().toString().trim();
        String strQuantity = etQuantity.getText().toString().trim();
        String strPrice = etPrice.getText().toString().trim();

        boolean isSucces = true;

        if (productName.isEmpty()){
            String message = getString(R.string.requeried);
            etProduct.setError(message);
            isSucces = false;
        }if (strQuantity.isEmpty()){
            String message = getString(R.string.requeried);
            etQuantity.setError(message);
            isSucces = false;
        }if (strPrice.isEmpty()){
            String message = getString(R.string.requeried);
            etPrice.setError(message);
            isSucces = false;
        }

        if (isSucces){
            int price = Integer.parseInt(strPrice);
            float quantity = Float.parseFloat(strQuantity);
            String id = UUID.randomUUID().toString();
            Product product = new Product(id, productName, description, quantity, price);
            addProductInDB(product);
        }
    }

    private void addProductInDB(Product product) {
        realm.beginTransaction();
        realm.copyToRealmOrUpdate(product);
        realm.commitTransaction();

        openMainActivity();
    }

    private void openMainActivity() {
        String message = getString(R.string.saved);
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
        Intent intent = new Intent( this, MainActivity.class);
        startActivity(intent);
    }




}
